import UIKit

/*
 Buenas la tarea para la siguiente clase es:
 implementar ese metodo. Tomen en cuenta q puedo mandar operaciones aritmeticas.
 y puedo mandar cualquier cosa
 
 func ejecutar(laOperacion operacion:String, numeroA:Double, numeroB:Double)-> Double{
 }
 */

enum OperacionesAritmeticas: String{
    case suma = "+"
    case resta = "-"
    case multiplicacion = "*"
    case division = "/"
    case potencia = "^"
    
    func operar(NumeroA a:Double, NumeroB b:Double) -> Double {
        switch self {
        case .suma:
            return a + b
        case .resta:
            return a - b
        case .multiplicacion:
            return a * b
        case .division:
            return a / b
        case .potencia:
            return pow(a, b)
        }
    }
}


func ejecutar(laOperacion operacion:String, numeroA:Double, numeroB:Double)->Double{
    
    guard let operacionEvaluada = OperacionesAritmeticas(rawValue: operacion) else {
        print("No existe la operacion indicada")
        return 0
    }
    
    let resultado = operacionEvaluada.operar(NumeroA: numeroA, NumeroB: numeroB)
    
    print("El resultado de la operacion \(operacionEvaluada) es: \(resultado)")
    
    return resultado
}

ejecutar(laOperacion: "+", numeroA: 2, numeroB: 3)

